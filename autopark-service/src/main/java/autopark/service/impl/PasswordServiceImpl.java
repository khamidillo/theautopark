package autopark.service.impl;

import autopark.dao.IPasswordResetTokenDAO;
import autopark.dao.IUserDAO;
import autopark.domain.PasswordResetToken;
import autopark.domain.User;
import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;
import autopark.service.AutoparkServiceException;
import autopark.service.IEmailSender;
import autopark.service.IPasswordService;
import autopark.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * 22.03.2016.
 */

@Service
public class PasswordServiceImpl implements IPasswordService {

    @Autowired
    private IEmailSender emailSender;

    @Autowired
    private IPasswordResetTokenDAO tokenDAO;

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @Autowired
    IUserDAO userDAO;

    @Override
    @Transactional
    public void requestResetPassword(PasswordResetTokenDTO tokenDTO, String baseURL) throws AutoparkServiceException{
        String tokenValue = UUID.randomUUID().toString();

        PasswordResetToken tokenEntity = new PasswordResetToken();
        tokenEntity.setDate(tokenDTO.getTokenDate());
        tokenEntity.setEmail(tokenDTO.getEmail());
        tokenEntity.setTokenValue(tokenValue);
        tokenEntity.setIpAddress(tokenDTO.getRemoteAddress());
        tokenEntity.setUserAgent(tokenDTO.getUserAgent());

        tokenDAO.save(tokenEntity);

        User user = userDAO.getByEmail(tokenDTO.getEmail());
        if(user != null){
            tokenDTO.setName(user.getName());
        }else{
            throw new AutoparkServiceException("user not found!");
        }

        emailSender.sendPasswordResetMail(baseURL, tokenDTO);
    }

    @Override
    @Transactional
    public void finishResetPassword(UserDTO userDTO, String baseURL) {
        userService.updateUserPassword(userDTO, baseURL);
        emailSender.sendSuccessPasswordResetMail(baseURL, userDTO);

    }
}
