package autopark.service.impl;

import autopark.dao.IRoleUserDAO;
import autopark.dao.IUserDAO;
import autopark.domain.ImageFile;
import autopark.domain.Role;
import autopark.domain.RoleUser;
import autopark.domain.User;
import autopark.dto.ImageFileDTO;
import autopark.dto.PrincipalUser;
import autopark.dto.RoleDTO;
import autopark.dto.UserDTO;
import autopark.service.AutoparkServiceException;
import autopark.service.IEmailSender;
import autopark.service.IUserService;
import autopark.service.ImageService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("myUserService")
public class UserServiceImpl implements IUserService, UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private IUserDAO userDAO;

    @Autowired
    private IRoleUserDAO roleUserDAO;

    @Autowired
    @Qualifier("baseEmailSender")
    private IEmailSender emailSender;

    @Autowired
    @Qualifier("encoder")
    private PasswordEncoder passwordEncoder;

    @Autowired
    ImageService imageService;

    @Transactional
    public boolean createUser(final UserDTO dto, final String baseURL){
        User user = userDAO.getByEmail(dto.getEmail());

        if(user != null){
            return false;
        }
        user = DTOAssembler.assembleUser(dto);
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setCreationDate(new Date());

//        if(dto.getImageFileDTO() != null){
//            try {
//                ImageFileDTO tfd = dto.getImageFileDTO();
//                ImageFile imageFile = imageService.uploadFile(tfd);
//                if(imageFile!=null){
//                    user.setMyPhotoImageFileId(imageFile.getId());
//                }
//            } catch (AutoparkServiceException e) {
//                log.error("cannot store image",e);
//            }
//        }

        Long userId = userDAO.save(user);

        RoleUser roleUser = new RoleUser();
        roleUser.setUSER_ID(userId);
        roleUser.setROLE_ID(Role.ROLE_USER_ID);
        roleUserDAO.save(roleUser);

        //send email
        new Thread(){
            public void run() {
                emailSender.sendCreationEmail(baseURL, dto);
            }
        }.start();
        return true;
    }


    @Override
    public PrincipalUser loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userDAO.getByEmail(s);
        return assemblePrincipalUser(user,false);
    }

    public UserDTO getUserByEmail(String email){
        User user = userDAO.getByEmail(email);
        if(user == null) {
            return null;
        }
        return DTOAssembler.assembleUserDTO(user);
    }


    public boolean processLostPassword(String ourServer, String email) {
        return false;
    }


    @Override
    public boolean updateUserPassword(UserDTO userDTO, String baseURL) {
        User user = userDAO.getByEmail(userDTO.getEmail());
        if(user == null){
            return false;
        }
        user.setPassword(userDTO.getPassword());
        /*userDTO.getPassword();*/
        userDAO.update(user);

        return true;

    }


    public PrincipalUser assemblePrincipalUser(User user, Boolean facebook){
        if(user != null){
            PrincipalUser springUser = new PrincipalUser();
            springUser.setEnabled(true);
            springUser.setPassword(user.getPassword());
            springUser.setUsername(StringUtils.isNotEmpty(user.getEmail()) ? user.getEmail() : user.getFacebookId());
            springUser.setFacebook(facebook);
            if(user.getAuthorities() != null){
                List<RoleDTO> roles = new ArrayList<RoleDTO>();
                for(Role role: user.getAuthorities()){
                    RoleDTO dto = new RoleDTO();
                    dto.setAuthority(role.getAuthority());
                    roles.add(dto);
                }
                springUser.setAuthorities(roles);
            }
            return springUser;
        }
        return null;
    }

    public UserDTO getUser() {
        //сделать здесь получение юзера
        PrincipalUser springUser = (PrincipalUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = null;
        if(springUser.getFacebook()!=null && springUser.getFacebook()){
            user = userDAO.getByFacebookId(springUser.getUsername());
        }
        if(user == null){
            user = userDAO.getByEmail(springUser.getUsername());
        }

        if(user != null) {
            return DTOAssembler.assembleUserDTO(user);
        }
        return null;
    }

    public User getCurrentUser() throws AutoparkServiceException{
        User user = null;
        try {
            PrincipalUser springUser = (PrincipalUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            user = userDAO.getByEmail(springUser.getUsername());
            if(user == null){
                user = userDAO.getByFacebookId(springUser.getUsername());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new AutoparkServiceException("Not authenticated user");
        }
        return user;
    }


    @Transactional
    public boolean updateUser(UserDTO userDTO) {
        User user = userDAO.getByEmail(userDTO.getEmail());
        if(user == null)
        {
            return false;
        }

            user.setName(userDTO.getName());
            user.setBirthday(userDTO.getBirthday());
            user.setPhone(userDTO.getPhone());


        if(userDTO.getImageFileDTO() != null){
            try {
                ImageFileDTO tfd = userDTO.getImageFileDTO();
                ImageFile imageFile = imageService.uploadFile(tfd);
                if(imageFile!=null){
                    user.setMyPhotoImageFileId(imageFile.getId());
                }
            } catch (AutoparkServiceException e) {
                log.error("cannot store image",e);
            }
        }

            userDAO.save(user);

        return true;
    }
@Transactional
    public boolean changeUserPassword(UserDTO userDTO) {
        User user = null;
        try {
            user = getCurrentUser();//взят юзер из сессии который залогеный
        } catch (AutoparkServiceException e) {
            e.printStackTrace();
        }
        if (user == null)
        {
           return false;
        }


        if(passwordEncoder.matches(userDTO.getPassword(), user.getPassword()))//если пароли совпали то сохраняем
        {
            if (userDTO.getPasswordRetry().equals(userDTO.getNewPassword())) {
                user.setPassword(passwordEncoder.encode(userDTO.getNewPassword()));
            }else{
                return false;
            }

        }else{
            return false;
        }

            return true;
    }


}