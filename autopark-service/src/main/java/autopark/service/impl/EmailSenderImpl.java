package autopark.service.impl;

import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;
import autopark.service.IEmailSender;
import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring3.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.util.Locale;

//aupark@yandex.ru
//aupark!123 - password

//aupark2@gmail.com
//aupark!123 - password

@Service
@Qualifier("baseEmailSender")
public class EmailSenderImpl implements IEmailSender {

    private final Logger log = LoggerFactory.getLogger(EmailSenderImpl.class);


    @Autowired
    private JavaMailSenderImpl javaMailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;


    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to);
            message.setFrom("aupark@yandex.ru");
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent e-mail to User '{}'", to);
        } catch (Exception e) {
            log.warn("E-mail could not be sent to user '{}', exception is: {}", to, e.getMessage());
        }
    }

    @Async
    public void sendPasswordResetMail(String baseUrl, PasswordResetTokenDTO tokenDTO) {
        log.debug("Sending password reset e-mail to '{}'", tokenDTO.getEmail());
        //Locale locale = Locale.forLanguageTag("en"); // TODO
        Context context = new Context();
        context.setVariable("user", tokenDTO);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("passwordResetEmail.html", context);
        String subject = "Reset Subject";///messageSource.getMessage("email.reset.title", null, locale);
        sendEmail(tokenDTO.getEmail(), subject, content, false, true);
    }


    @Async
    public void sendActivationEmail(String baseUrl, UserDTO user) {
        log.debug("Sending activation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag("en"); //TODO
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("activationEmail.html", context);
        String subject = "Activation Subject";//messageSource.getMessage("email.activation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendCreationEmail(String baseUrl, UserDTO user) {
        log.debug("Sending creation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag("en"); //TODO
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("creationEmail.html", context);
        String subject = "Creation Subject";/*messageSource.getMessage("email.activation.title", null, locale);*/
        sendEmail(user.getEmail(), subject, content, false, true);
    }

    @Async
    public void sendSuccessPasswordResetMail(String baseUrl, UserDTO user) {
        log.debug("Sending password reset success to '{}'", user.getEmail());
        Locale locale = Locale.forLanguageTag("en"); //TODO
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("baseUrl", baseUrl);
        String content = "Your password was successfully changed"; /*templateEngine.process("activationEmail.html", context);*/
        String subject = "Reset Subject"; //messageSource.getMessage("email.activation.title", null, locale);
        sendEmail(user.getEmail(), subject, content, false, true);

    }
}