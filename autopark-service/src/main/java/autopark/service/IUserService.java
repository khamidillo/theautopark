package autopark.service;

import autopark.domain.User;
import autopark.dto.PrincipalUser;
import autopark.dto.UserDTO;

public interface IUserService {
    boolean createUser(final UserDTO dto, final String baseURL);
    UserDTO getUserByEmail(String email);

    /**
     * Метод принимает информацию о нашем сервере, email
     * Достает пользователя, создает ссылку, и посылает на этот email ссылку.
     * @param ourServer - server info
     * @param email - email пользователя
     * @return
     */
    boolean processLostPassword(String ourServer,String email);

    boolean updateUserPassword(UserDTO userDTO, String baseURL);

    PrincipalUser assemblePrincipalUser(User user, Boolean facebook);

    UserDTO getUser();

    User getCurrentUser() throws AutoparkServiceException;

    boolean updateUser(UserDTO userDTO);

    boolean changeUserPassword(UserDTO userDTO);
}
