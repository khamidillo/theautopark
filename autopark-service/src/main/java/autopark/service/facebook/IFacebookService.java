package autopark.service.facebook;

import autopark.service.AutoparkServiceException;
import autopark.service.facebook.impl.FBInfoDTO;

/**
 * Created by  01 on 15.02.2016.
 */
public interface IFacebookService {
    String getFBLoginUrl();
    String getToken(String code) throws AutoparkServiceException;
    FBInfoDTO getMe(String token) throws AutoparkServiceException;
    void processLogin(String token) throws AutoparkServiceException;

}
