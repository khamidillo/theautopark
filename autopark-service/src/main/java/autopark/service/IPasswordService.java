package autopark.service;

import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;

/**
 * 22.03.2016.
 */
public interface IPasswordService {

    void requestResetPassword(PasswordResetTokenDTO tokenDTO, String baseURL) throws AutoparkServiceException;

    void finishResetPassword(UserDTO userDTO, String baseURL);


}
