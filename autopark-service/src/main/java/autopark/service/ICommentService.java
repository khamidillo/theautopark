package autopark.service;

import autopark.domain.Comment;
import autopark.dto.CommentDTO;

/**
 * 22.03.2016.
 */
public interface ICommentService {
    Comment createCarComment(CommentDTO dto);
}
