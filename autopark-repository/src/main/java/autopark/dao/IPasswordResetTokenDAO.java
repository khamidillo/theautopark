package autopark.dao;

import autopark.domain.PasswordResetToken;

/**
 * 21.03.2016.
 */
public interface IPasswordResetTokenDAO extends IRootDAO<PasswordResetToken>{

    PasswordResetToken getTokenByValue(String tokenValue);

    PasswordResetToken getTokenByEmail(String email);
}
