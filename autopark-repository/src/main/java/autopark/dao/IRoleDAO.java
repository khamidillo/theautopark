package autopark.dao;


import autopark.domain.Role;

public interface IRoleDAO extends IRootDAO<Role> {

    Role getUserRole();

}