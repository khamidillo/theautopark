package autopark.web.controller;

import autopark.dto.DemandDTO;
import autopark.service.IDemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class DemandsController {

    @Autowired
    private IDemandService demandService;


    @RequestMapping(value = {"/demands"})
    public String handle(ModelMap modelMap) {

        List<DemandDTO> list = demandService.getDemands();


        modelMap.put("demands",list);

        return "/WEB-INF/content/demands.jsp";
    }






}



