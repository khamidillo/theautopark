package autopark.web.controller.oauth;

import autopark.service.AutoparkServiceException;
import autopark.service.facebook.IFacebookService;
import autopark.web.controller.Constants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FacebookController {
    private final Logger log = LoggerFactory.getLogger(FacebookController.class);

    @Autowired
    private IFacebookService facebookService;


    @RequestMapping(value = "/oauth/fblogin")
    public String handle(@RequestParam(required = false) String code) {
        if (StringUtils.isEmpty(code)) {
            log.error("Failed to complete authentication code is empty");
            //todo something
        }

        try {
            facebookService.processLogin(code);
        } catch (AutoparkServiceException e) {
            e.printStackTrace();
            //todo something, put error on page
        }


        return Constants.REDIRECT+Constants.profile;
    }



}



