package autopark.web.controller;

/**
 * Created by aro on 07.03.2016.
 */
public class Constants {
    public static final String COMMAND = "command";
    public static final String USER = "user";
    public static final String LIST = "list";
    public static final String REDIRECT = "redirect:/";
    public static final String profile = "profile";
    public static final String PATH_DELIMITER = "/";



    public static final String SIGNUP = "/WEB-INF/content/signup.jsp";
    public static final String ENTER  = "/WEB-INF/content/enter.jsp";
    public static final String PROFILE = "/WEB-INF/content/profile/profile.jsp";

    public static final String LOST_PASSWORD = "/WEB-INF/content/profile/lostpassword.jsp";
    public static final String RECOVER_PASSWORD = "/WEB-INF/content/recover.jsp";

    public static final String CHANGE_PASSWORD = "/WEB-INF/content/profile/changepassword.jsp";


}
