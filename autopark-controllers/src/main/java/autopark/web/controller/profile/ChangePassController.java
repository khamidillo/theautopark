package autopark.web.controller.profile;

import autopark.dto.UserDTO;
import autopark.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ChangePassController {
    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;


    @RequestMapping(value = "/changepassword", method = RequestMethod.GET)
    public String changePasswordGet() {
        return "/WEB-INF/content/profile/changepassword.jsp";
    }

    @RequestMapping(value = "/changepassword", method = RequestMethod.POST)
    public String lostPasswordPost(@ModelAttribute("command")UserDTO userDTO,
                                    ModelMap modelMap) {

        boolean valid = userService.changeUserPassword(userDTO);

        if (!valid) {
            modelMap.put("res", -2);
            return "/WEB-INF/content/profile/changepassword.jsp";
        }

        modelMap.put("res", 0);

        return "/WEB-INF/content/profile/changepassword.jsp";
    }

    @ModelAttribute("command")
    public UserDTO createEmployeeModel() {
        return new UserDTO();
    }
    }






