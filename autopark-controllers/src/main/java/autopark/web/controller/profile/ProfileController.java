package autopark.web.controller.profile;

import autopark.domain.Role;
import autopark.dto.ImageFileDTO;
import autopark.dto.UserDTO;
import autopark.service.IUserService;
import autopark.web.auth.RequiresAuthentication;
import autopark.web.controller.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by aro on 07.03.2016.
 */
@Controller
public class ProfileController {
    private final static Logger logger = LoggerFactory.getLogger(ProfileController.class);

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    @RequiresAuthentication({Role.ROLE_USER, Role.ROLE_ADMIN})
    public String handle(ModelMap modelMap) {
        logger.debug("Profile entered");

        UserDTO userDTO = userService.getUser();

        if (logger.isDebugEnabled()) {
            logger.info("Profile entered, user={}", userDTO.getId());
        }

        modelMap.put(Constants.COMMAND, userDTO);
        return Constants.PROFILE;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String handle(@ModelAttribute("command") @Validated UserDTO userDTO,
                         BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {

        if (request instanceof MultipartRequest) {
            //myphoto
            MultipartRequest mRequest = (MultipartRequest) request;
            List<MultipartFile> files = mRequest.getMultiFileMap().get("myPhoto");
            if (files != null && files.size() > 0) {
                ImageFileDTO imageFileDTO = new ImageFileDTO();
                MultipartFile mpf = files.get(0);
                if (mpf != null && mpf.getSize() > 0) {
                    imageFileDTO.setMpf(mpf);
                    userDTO.setImageFileDTO(imageFileDTO);
                }
            }
        }
        boolean valid = userService.updateUser(userDTO);

        return Constants.PROFILE;
    }

    @ModelAttribute("command")
    public UserDTO createEmployeeModel() {
        return new UserDTO();
    }


}
