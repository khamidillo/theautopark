package autopark.web.controller.profile;

import autopark.dto.PasswordResetTokenDTO;
import autopark.dto.UserDTO;
import autopark.service.AutoparkServiceException;
import autopark.service.IPasswordService;
import autopark.service.ITokenService;
import autopark.service.IUserService;
import autopark.web.controller.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Locale;

/**
 * Created by  01 on 14.03.2016.
 */
@Controller
public class LostPasswordController {

    @Autowired
    @Qualifier("myUserService")
    private IUserService userService;

    @Autowired
    private ITokenService tokenService;

    @Autowired
    private IPasswordService passwordService;

    @Autowired
    @Qualifier("emailValidator")
    private Validator validator;


    @RequestMapping(value = "/lostpassword", method = RequestMethod.GET)
    public String lostPasswordGet() {
        return Constants.LOST_PASSWORD;
    }


    @RequestMapping(value = "/lostpassword", method = RequestMethod.POST)
    public String lostPasswordPost(@ModelAttribute("command") @Validated PasswordResetTokenDTO tokenDTO,
                                   BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {


        if (bindingResult.hasErrors()) {
            modelMap.put(Constants.COMMAND, tokenDTO);
            return Constants.LOST_PASSWORD;
        }

        tokenDTO.setRemoteAddress(request.getRemoteAddr());
        tokenDTO.setUserAgent(request.getHeader("user-agent"));
        tokenDTO.setTokenDate(new Date());

        try {
            passwordService.requestResetPassword(tokenDTO, assembleBaseURL(request));
        } catch (AutoparkServiceException e) {
            e.printStackTrace();
            //todo create error key and put it on page
        }

        return Constants.LOST_PASSWORD;
    }


    @RequestMapping(value = "/reset/finish", method = RequestMethod.GET)
    public String handleRecoverGet(Locale locale, ModelMap modelMap,
                                   @RequestParam(value = "key") String key,
                                   @RequestParam(value = "email") String email) {

        /*https://stackoverflow.com/account/recover?recoveryToken=
        vIntVgAAAABZAQBzrF9PIg%3d%3d%7c1bfd1214c4e4f3dea8e2f2e47e5da77fbcf9c07836594c495c9f14491dec9482*/

        if (tokenService.exist(email, key)) {
            return Constants.RECOVER_PASSWORD;
        }

        return "redirect:/enter";

    }


    @RequestMapping(value = "/reset/finish", method = RequestMethod.POST)
    public String handleRecoverPost(@ModelAttribute("command") @Validated UserDTO userDTO,
                                    @RequestParam(value = "key") String key,
                                    @RequestParam(value = "email") String email,
                                    BindingResult bindingResult, ModelMap modelMap, HttpServletRequest request) {


        if (bindingResult.hasErrors()) {
            return Constants.RECOVER_PASSWORD;
        }

        String baseURL = assembleBaseURL(request);
        userDTO.setEmail(email);
        userService.updateUserPassword(userDTO, baseURL);

        modelMap.put(Constants.COMMAND, userDTO);
        return Constants.RECOVER_PASSWORD;

    }


    @InitBinder
    private void binder(WebDataBinder binder) {
        //Add validator
        binder.setValidator(validator);
    }

    private String assembleBaseURL(HttpServletRequest request) {
        return new StringBuilder().
                append(request.getScheme()).
                append("://").append(request.getServerName()).
                append(":").append(request.getServerPort()).
                append(request.getContextPath()).
                toString();
    }


}
