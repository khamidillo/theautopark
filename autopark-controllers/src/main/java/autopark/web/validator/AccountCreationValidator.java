package autopark.web.validator;

import autopark.dto.UserDTO;
import autopark.utils.ApValidationUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by aro on 07.03.2016.
 */
@Component("accountCreationValidator")
public class AccountCreationValidator implements Validator {

    public boolean supports(Class<?> aClass) {
        return UserDTO.class.equals(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "fio.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "email.required");
        //ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "phone.required");

        UserDTO dto = (UserDTO) o;
        if(!ApValidationUtils.verifyPhone(dto.getPhone())){
            errors.rejectValue("phone","phone.incorrect");
        }

        if(!ApValidationUtils.verifyPassword(dto.getPassword())){
            errors.rejectValue("password","password.incorrect");
        }

        if(dto.getPassword()!=null && !dto.getPassword().equals(dto.getPasswordRetry())){
            errors.rejectValue("passwordRetry","retry.incorrect");
        }



    }

}
