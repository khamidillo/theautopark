package autopark.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by  01 on 21.12.2015.
 */
@Entity
@Table(name = "ap_comment")
public class Comment extends Root {
    private String description;
    private Date creationDate;
    private CommentEntityEnum entity;
    private Long entityId;
    private String commentatorName;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Enumerated(value = EnumType.STRING)
    public CommentEntityEnum getEntity() {
        return entity;
    }

    public void setEntity(CommentEntityEnum entity) {
        this.entity = entity;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getCommentatorName() {
        return commentatorName;
    }

    public void setCommentatorName(String commentatorName) {
        this.commentatorName = commentatorName;
    }
}

