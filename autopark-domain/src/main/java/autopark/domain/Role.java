package autopark.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ap_role")
public class Role extends Root {

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER  = "ROLE_USER";

    public static final Long ROLE_ADMIN_ID = 1l;
    public static final Long ROLE_USER_ID  = 2l;

    private String authority;

    @Column(unique = true, nullable = false)
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}
