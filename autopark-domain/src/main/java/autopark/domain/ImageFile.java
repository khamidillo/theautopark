package autopark.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by aro on 09.04.2016.
 */
@Entity
@Table(name = "ap_image_file")
public class ImageFile extends Root {
    private String fileName;
    private String pathfolder;
    private Date date;
    private Long userId;
    private String userEmail;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPathfolder() {
        return pathfolder;
    }

    public void setPathfolder(String pathfolder) {
        this.pathfolder = pathfolder;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
