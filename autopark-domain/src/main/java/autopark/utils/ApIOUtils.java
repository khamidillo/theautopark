package autopark.utils;

import org.apache.commons.io.FileUtils;

import java.io.*;

/**
 * Created by aro on 09.04.2016.
 */
public class ApIOUtils {
    public static boolean copyDir(String from, String to) {
        try {
            File FROM = new File(from);
            File TO = new File(from);
            if(FROM.exists() && TO.exists()){
                FileUtils.copyDirectory(FROM, TO);
            }else{
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    // Returns the contents of the file in a byte array.
    public static byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int)length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        // Close the input stream and return bytes
        is.close();
        return bytes;
    }
    public static String getFileExtension(String fileName) {
        int dotIndex = fileName.lastIndexOf(".");
        String ext = "";
        if (dotIndex > 0) {
            ext = fileName.substring(dotIndex + 1);
        }
        return ext;
    }


    // Copies src file to dst file.
    // If the dst file does not exist, it is created
    public static void saveInputStreamAsFile(InputStream in, String to) throws IOException {
        copyFile(in, new FileOutputStream(to));
    }

    // Copies src file to dst file.
    // If the dst file does not exist, it is created
    public static void saveBytesAsFile(byte[] bytes, String to) throws IOException {
        if (bytes != null) {
            FileOutputStream fos = new FileOutputStream(to);
            fos.write(bytes);
            fos.close();
        }
    }

    public static void copyFile(InputStream in, OutputStream to) throws IOException {
        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            to.write(buf, 0, len);
        }
        in.close();
        to.close();
    }

    public static String toEnc(String s,String enc){
        byte[] fileNameBytes = new byte[0];
        try {
            fileNameBytes = s.getBytes(enc);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        String dispositionFileName = "";
        for (byte b: fileNameBytes) dispositionFileName += (char)(b & 0xff);
        return dispositionFileName;

    }


}

