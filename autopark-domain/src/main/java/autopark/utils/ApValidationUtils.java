package autopark.utils;

import org.apache.commons.lang.StringUtils;

/**
 * Created by aro on 07.03.2016.
 */
public class ApValidationUtils {

    public static final boolean verifyPhone(String s){
        if(StringUtils.isEmpty(s)){
            return false;
        }
        //validate phone numbers of format "1234567890"
        if (s.matches("\\d{10}")) return true;
            //validating phone number with -, . or spaces
        else if(s.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
            //validating phone number with extension length from 3 to 5
        else if(s.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
            //validating phone number where area code is in braces ()
        else if(s.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
            //return false if nothing matches the input
        else return false;
    }

    public static final boolean verifyPassword(String s){
        if(StringUtils.isEmpty(s)){
            return false;
        }
        if(s.length()<6){
            return false;
        }
        return true;
    }

}
