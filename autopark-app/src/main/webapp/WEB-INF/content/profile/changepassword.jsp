<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="/WEB-INF/content/tiles/head.jsp" %>

    link rel="stylesheet" type="text/css" href="/styles/changepassword.css" />
</head>
    <body>
    <tags:headerV2 activeProfile="active"/>
    <div id="wrap">

    <div class="container">

            <springForm:form action="/changepassword" method="post" commandName="command" cssClass="form-changepass">
                <br/>
                <c:choose>
                    <c:when test="${res==-2}">
                        <span class="red">Ошибка!Неверно введен пароль или пароли не совпадают :(</span>
                        <br/>
                    </c:when>
                    <c:when test="${res==0}">
                        <span class="green">Пароль изменен!</span>
                        <br/>
                    </c:when>
                </c:choose>

            <h2 class="form-signin-heading">Введите Ваш пароль</h2>
            <label for="inputPassword" class="sr-only">Password</label>
            <input name="password"class="form-control" placeholder="Ваш пароль" type="password"  id="inputPassword" value="" required >


            <h2 class="form-signin-heading">Введите Ваш новый пароль</h2>
            <label for="inputPassword" class="sr-only">Password</label>
            <input name="newpassword" class="form-control" placeholder="Ваш новый пароль" type="password" id="inputNewPassword" value="" required><div><springForm:errors path="password" cssClass="error red" /></div>

            <h2 class="form-signin-heading">Повторите новый пароль</h2>
            <label for="inputPassword" class="sr-only">Password</label>
            <input name="passwordRetry" class="form-control" placeholder="Ваш новый пароль" type="password" id="inputNewPassword2" value="" required>


            <button class="btn btn-lg btn-primary btn-block" type="submit">Сохранить</button>



        </springForm:form>


    </div>

    </div>

    <hr>
    <%@ include file="/WEB-INF/content/tiles/head.jsp" %>
    <script src="https://www.google.com/recaptcha/api.js"></script>


    </body>
</html>