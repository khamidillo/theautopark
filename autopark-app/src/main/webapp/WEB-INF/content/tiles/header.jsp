<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>

<div class="header">
    <div class="header-content">
        <%@ include file="signin.jsp" %>

        <div>
            Приложение Автопарк, версия 1.0
        </div>
        <div>
            Автор: ${author}
        </div>

        <div>
            Время запроса к базе данных в миллисекундах: ${millis}
        </div>
    </div>

</div>
