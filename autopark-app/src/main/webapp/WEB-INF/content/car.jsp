<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="tiles/head.jsp" %>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


    <link rel="stylesheet" type="text/css" href="/styles/car.css" />
    <script src="/js/car.js"></script>

</head>


<body>
    <tags:headerV2 activeCars="active"/>
    <div id="wrap">

        <div class="container">
            <br/>
            <div>
                <h2>Описание автомобиля</h2>

                <div>
                    <span title="Марка">Марка: ${car.vendor}</span>
                </div>
                <div>
                    Модель: ${car.model}
                </div>
                <div>
                    Год выпуска: ${car.year}
                </div>
                <div>
                    Тип ТС: ${car.type}
                </div>
                <div>
                    Тип ТС: ${car.capacity}
                </div>
            </div>
            <br/>
            <br/>
            <div class="">
                <h2>Комментарии</h2>
                <br/>
                <a href="#" class="btn btn-success btn-lg addcom">
                    <span class="glyphicon glyphicon-plus"></span>Добавить комментарий
                </a>



                <c:if test="${car.comments!=null}">
                    <c:forEach items="${car.comments}" var="item">
                        <tags:carcomment comment="${item}"/>
                    </c:forEach>
                </c:if>


            </div>


        </div>
    </div>
    <hr>
    <%@ include file="tiles/footer.jsp" %>

</body>



<div id="dialog-form" title="Добавить отзыв">
    <p class="validateTips">Все поля обязательные.</p>

    <form action="/comment/add" method="post">
        <fieldset>
            <input type="hidden" value="${car.id}" name="entityId" id = "ac-id">
            <p>Текст отзыва</p>
            <textarea rows="8" name="description"  placeholder="Текст отзыва" class="form-control" id="ac-description"></textarea>

            <br/><br/>
            <p>Ваше имя</p>
            <input name="commentatorName" type="text" placeholder="Ваше имя" class="form-control" value="" id="ac-name">
            <!-- Allow form submission with keyboard without duplicating the dialog button -->

            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
    </form>
</div>






