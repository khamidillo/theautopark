<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <%@ include file="tiles/head.jsp" %>

    <link rel="stylesheet" type="text/css" href="/styles/enter.css" />
</head>
    <body>
    <tags:headerV2 activeProfile="active"/>
    <div id="wrap">

    <div class="container">
        <br/>
        <%--<span class="title2">Вход в Ваш аккаунт приложения Автопарк</span><br><br>--%>

        <form action="<c:url value="/j_spring_security_check"></c:url>" method="post" name="loginForm" class="form-signin">
            <h2 class="form-signin-heading">Введите Ваш email и пароль</h2>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input name="username" id="inputEmail" placeholder="Ваш Email" class="form-control" type="text"  value="" autocomplete="on" required autofocus>

            <label for="inputPassword" class="sr-only">Password</label>
            <input name="password" class="form-control" placeholder="Ваш пароль" type="password" id="inputPassword" value="" required>


            <div class="g-recaptcha" data-sitekey="
                          6LeYRBwTAAAAACzLkHLLfgZlXETfwDbC3w9xWq0A"></div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>

            <div class="checkbox">
                <label>
                    <a href="/lostpassword" class="blueLink">Не можете войти в свой аккаунт?</a>
                </label>
            </div>
            <div class="checkbox">
                <label>
                    Нет еще аккаунта в Автопарке? <a href="/signup" class="blueLink">Регистрация здесь.</a>
                </label>
            </div>

            <a href="${fburl}" class="btn btn-block btn-social btn-facebook">
                <span class="fa fa-facebook"></span> Log in with Facebook
            </a>


        </form>


    </div> <!-- /container -->

    </div>

    <hr>
    <%@ include file="tiles/footer.jsp" %>
    <script src="https://www.google.com/recaptcha/api.js"></script>


    </body>
</html>