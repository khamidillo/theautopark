<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ attribute name="comment" type="autopark.dto.CommentDTO" required="true" %>
<div class="comment-row">
    <div class="comment-text">
        <c:out value="${comment.description}"/>
    </div>
    <div class="comment-date-who">
        автор <c:out value="${comment.commentatorName}"/>;
        дата <fmt:formatDate value="${command.creationDate}" pattern="dd.MM.yyyy hh:mm" />
    </div>
</div>
<br/>

