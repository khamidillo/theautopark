<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ attribute name="activeCars" type="java.lang.String" required="false" %>
<%@ attribute name="activeDemands" type="java.lang.String" required="false" %>
<%@ attribute name="activeAnn" type="java.lang.String" required="false" %>
<%@ attribute name="activeProfile" type="java.lang.String" required="false" %>


<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="/">Автопарк.</a>
        </div>

        <ul class="nav navbar-nav">
            <li class="${activeCars}"><a href="/main">Автомобили</a></li>
            <li class="${activeDemands}"><a href="/demands">Заявки</a></li>
            <li class="${activeAnn}"><a href="/announcements">Объявления</a></li>
            <li class="${activeProfile}"><a href="/profile">Профиль</a></li>
        </ul>


        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <sec:authorize access="isAuthenticated()">
                    <a href="/profile" class="btn btn-success">Профиль</a>
                    <a href="/j_spring_security_logout" class="btn btn-success">Выход</a>
                </sec:authorize>
                <sec:authorize access="isAnonymous()">
                    <a href="/enter" class="btn btn-success">Вход</a>
                </sec:authorize>
            </form>

        </div><!--/.navbar-collapse -->
    </div>
</nav>
